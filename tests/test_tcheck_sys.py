import os
import sys
import pytest
import yaml
import shutil
import pygit2

import tcheck.tcheck_check as checks
import tcheck.tcheck_sys as sys
import tcheck.tcheck_git as git
import tcheck.tcheck_util as util
from tcheck.tcheck_exceptions import TcheckException

oldpath = os.getcwd()

key = 'testing_key'
regex = '.*?'
run = 'echo "testing"'
message = 'this is a test case'

class TestSys():
    @pytest.fixture(autouse=True)
    def setup(module, tmp_path):
        os.chdir(str(tmp_path))
        repo = pygit2.init_repository(os.getcwd())
        sys.do_init(False)
        module.config_dir = str(tmp_path) + '/.config/tcheck/'
        module.config_file = module.config_dir + 'checks.yaml'

    def remove_init(self):
        shutil.rmtree(os.getcwd() + '/.tcheck')

    def create_config_file(self):
        os.makedirs(self.config_dir)
        data = {
                'checks': {
                'test_1': {
                    'match'  : regex,
                    'run'    : run,
                    'message': message
                },
                'test_2': {
                    'match'  : regex,
                    'run'    : run,
                    'message': message
                }
            }
        }

        with open(self.config_file, 'w') as check:
            yaml.dump(data, check, default_flow_style=False)

    def create_test_check(self):
        checks.add(key, regex, run, message)

    def test_do_init(self):
        dir = util.get_tcheck_dir()
        assert os.path.exists(dir)

    def test_do_init_twice(self):
        try:
            sys.do_init(False)
        except TcheckException:
            dir = util.get_tcheck_dir()
            assert os.path.exists(dir)

    def test_do_init_has_config(self):
        self.create_config_file()
        self.remove_init()
        sys.do_init(True, config_file=self.config_file)
        dir = util.get_tcheck_dir()
        assert os.path.exists(dir)

    def test_do_init_no_config(self):
        self.remove_init()
        sys.do_init(True, config_file=self.config_file)
        dir = util.get_tcheck_dir()
        assert os.path.exists(dir)

    def test_do_init_unchecked(self):
        dir = util.get_tasks_dir() + '/unchecked'
        assert os.path.exists(dir)

    def test_do_init_complete(self):
        dir = util.get_tasks_dir() + '/complete'
        assert os.path.exists(dir)

    def test_do_init_failed(self):
        dir = util.get_tasks_dir() + '/failed'
        assert os.path.exists(dir)
    
    def test_do_remove(self):
        sys.do_remove()
        dir = util.get_tcheck_dir()
        assert not os.path.exists(dir)

    def test_update_check_rev(self):
        sys.update_check_rev()
        rev_file = util.get_tcheck_dir() + '/refs/' + git.get_branch_shorthand() + '/REV'
        with open(rev_file, "r") as f:
            rev = f.read()

        assert rev == '2' 

    def test_update_check_rev_twice(self):
        sys.update_check_rev()
        sys.update_check_rev()
        rev_file = util.get_tcheck_dir() + '/refs/' + git.get_branch_shorthand() + '/REV'
        with open(rev_file, "r") as f:
            rev = f.read()

        assert rev == '3' 

    def test_update_check_rev_failed_checks(self):
        self.create_test_check()
        checks.fail(key)
        sys.update_check_rev()
        rev_file = util.get_tcheck_dir() + '/refs/' + git.get_branch_shorthand() + '/REV'
        with open(rev_file, "r") as f:
            rev = f.read()

        assert rev == '2' 

    def test_update_check_rev_unchecked(self):
        sys.update_check_rev()
        dir = util.get_tasks_dir() + '/unchecked/2'
        assert os.path.exists(dir)

    def test_update_check_rev_complete(self):
        sys.update_check_rev()
        dir = util.get_tasks_dir() + '/complete/2'
        assert os.path.exists(dir)

    def test_update_check_rev_failed(self):
        sys.update_check_rev()
        dir = util.get_tasks_dir() + '/failed/2'
        assert os.path.exists(dir)

    def test_update_check_rev_uncheck_all(self):
        self.create_test_check()
        checks.complete(key)
        sys.update_check_rev(uncheck_all=True)
        old_file = util.get_tasks_dir() + '/complete/' + util.get_check_rev() + '/' + key
        file = util.get_tasks_dir() + '/unchecked/' + util.get_check_rev() + '/' + key
        assert os.path.isfile(file)
        assert not os.path.isfile(old_file)

