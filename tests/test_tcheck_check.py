import os
import sys
import pytest
import pygit2

import tcheck.tcheck_check as checks
import tcheck.tcheck_util as util
import tcheck.tcheck_sys as sys

oldpath = os.getcwd()

key = 'testing_key'
regex = '.*?'
run = 'echo "testing"'
message = 'this is a test case'

class TestCheck():
    @pytest.fixture(autouse=True)
    def setup(module, tmp_path):
        os.chdir(str(tmp_path))
        repo = pygit2.init_repository(os.getcwd())
        sys.do_init(False)

    def create_test_check(self, key_postfix='', regex=regex, run=run, message=message):
        checks.add(key + key_postfix, regex, run, message)

    def test_add(self):
        checks.add(key, regex, run, message)
        file = util.get_tasks_dir() + '/unchecked/' + util.get_check_rev() + '/' + key
        assert os.path.exists(file)

    def test_remove(self):
        self.create_test_check()
        status = util.get_status_from_key(key)
        checks.delete(key)
        file = util.get_tasks_dir() + '/' + status + '/' + util.get_check_rev() + '/' + key
        assert not os.path.exists(file)

    def test_fail(self):
        self.create_test_check()
        checks.fail(key)
        file = util.get_tasks_dir() + '/failed/' + util.get_check_rev() + '/' + key
        assert os.path.exists(file)

    def test_unchecked(self):
        self.create_test_check()
        checks.complete(key)
        checks.uncheck(key)
        file = util.get_tasks_dir() + '/unchecked/' + util.get_check_rev() + '/' + key
        assert os.path.exists(file)

    def test_complete(self):
        self.create_test_check()
        checks.complete(key)
        file = util.get_tasks_dir() + '/complete/' + util.get_check_rev() + '/' + key
        assert os.path.exists(file)

    def test_get_status_from_key(self):
        self.create_test_check()
        status = util.get_status_from_key(key)
        assert status == 'unchecked'

    def test_get_message(self):
        self.create_test_check()
        check_message = checks.get_message(key)
        assert check_message == message

    def test_get_run(self):
        self.create_test_check()
        check_run = checks.get_run(key)
        assert check_run == run

    def test_do_run(self):
        self.create_test_check()
        checks.do_run(key)
        file = util.get_tasks_dir() + '/complete/' + util.get_check_rev() + '/' + key
        assert os.path.exists(file)

    def test_do_match(self):
        self.create_test_check()
        matches = checks.do_match('this content will be matched')
        assert len(matches) > 0

    def test_do_match_multiple(self):
        self.create_test_check()
        self.create_test_check(key_postfix='_1', regex=['.*?', '.*?'])
        matches = checks.do_match('this content will be matched')
        assert len(matches) > 0

    def test_list(self):
        self.create_test_check(key_postfix='_unchecked')
        self.create_test_check(key_postfix='_complete')
        self.create_test_check(key_postfix='_failed')
        checks.complete(key + '_complete')
        checks.fail(key + '_failed')
        checks.list()

    def test_do_run(self):
        self.create_test_check()
        checks.run(key)

    def test_do_run_unchecked(self):
        self.create_test_check()
        checks.do_run_unchecked()

    def test_get_all_checks(self):
        self.create_test_check()
        keys = checks.get_all_checks()
        assert len(keys) > 0

    def test_get_failed_or_unchecked(self):
        self.create_test_check(key_postfix='_unchecked')
        self.create_test_check(key_postfix='_failed')
        checks.fail(key + '_failed')
        keys = checks.get_failed_or_unchecked()
        assert len(keys) > 0
    
    def test_get_complete_or_unchecked(self):
        self.create_test_check(key_postfix='_unchecked')
        self.create_test_check(key_postfix='_complete')
        checks.complete(key + '_complete')
        keys = checks.get_complete_or_unchecked()
        assert len(keys) > 0
    
    def test_get_complete_or_failed(self):
        self.create_test_check(key_postfix='_complete')
        self.create_test_check(key_postfix='_failed')
        checks.complete(key + '_complete')
        checks.fail(key + '_failed')
        keys = checks.get_complete_or_failed()
        assert len(keys) > 0

