import os
import sys
import pytest
import pygit2

import tcheck.tcheck_config as config
import tcheck.tcheck_sys as sys

oldpath = os.getcwd()

class TestConfig():
    @pytest.fixture(autouse=True)
    def setup(module, tmp_path):
        os.chdir(str(tmp_path))
        repo = pygit2.init_repository(os.getcwd())
        sys.do_init(False)
        module.config_dir = str(tmp_path) + '/.config/tcheck/'
        module.config_file = module.config_dir + 'checks.yaml'

    def create_config_file(self):
        os.makedirs(self.config_dir)
        with open(self.config_file, 'w') as f:
            f.write(' ')

    def test_get_config(self):
        self.create_config_file()
        config_data = config.get_config(self.config_file)
        assert not config_data == False

    def test_get_config_no_config(self):
        config_data = config.get_config(self.config_file)
        assert config_data == False

