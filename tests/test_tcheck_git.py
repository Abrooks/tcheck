import os
import pytest
import pygit2

import tcheck.tcheck_sys as sys
import tcheck.tcheck_util as util
import tcheck.tcheck_git as git

oldpath = os.getcwd()

class TestGit():
    @pytest.fixture(autouse=True)
    def setup(self, tmp_path):
        os.chdir(str(tmp_path))
        repo = pygit2.init_repository(os.getcwd())
        sys.do_init(False)
        self.git_dir = os.getcwd() + '/.git/'

    def test_write_head_from_git(self):
        tcheck_dir = util.get_tcheck_dir()
        git.write_head_from_git(tcheck_dir)
        
        with open(tcheck_dir + '/HEAD', 'r') as f:
            head = f.read()
            assert git.get_branch_shorthand() == head

    def test_write_hooks(self):
        git.write_hooks(self.git_dir)
        hook = self.git_dir + 'hooks/pre-commit'
        assert os.path.exists(hook)

    def test_write_hooks_twice(self):
        git.write_hooks(self.git_dir)
        git.write_hooks(self.git_dir)
        hook = self.git_dir + 'hooks/pre-commit'
        assert os.path.exists(hook)

    def test_write_hooks_content(self):
        git.write_hooks(self.git_dir)
        hook = self.git_dir + 'hooks/pre-commit'
        with open(hook, 'r') as f:
            hook_content = f.read()
            hook_content.find('tcheck hooks pre-commit')
            assert hook_content.find('tcheck hooks pre-commit') > -1

    def test_get_modified_files(self):
        git.get_modified_files()

    def test_pre_commit(self):
        git.pre_commit()

