import io
import re 
from setuptools import setup

with io.open("tcheck/__init__.py", "rt", encoding="utf8") as f:
    version = re.search(r"__version__ = \'(.*?)\'", f.read()).group(1)

setup(
    name="tcheck", 
    version=version,
    packages=["tcheck"], 
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'tcheck = tcheck.__main__:main'
        ]
    },
    install_requires=[
        'pygit2',
        'click',
        'pyyaml'
    ],
    python_requires=">=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*",
)
 
