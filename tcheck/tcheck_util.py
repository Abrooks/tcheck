import os
import click

from .tcheck_git import get_branch_shorthand, get_workdir
from .tcheck_exceptions import TcheckException
from .tcheck_errors import TcheckErrors

from shutil import copytree, copyfile

def get_tcheck_dir():
    workdir = get_workdir()
    tcheck_dir = workdir + '.tcheck'

    return tcheck_dir

def get_tasks_dir():
    return get_tcheck_dir() + '/refs/' + get_branch_shorthand() + '/tasks'

def get_check(key):
    rev = get_check_rev()
    status = get_status_from_key(key)

    try:
        check = get_tasks_dir() + '/' + status + '/' + rev + '/' + key
    except TypeError:
        check = get_tasks_dir() + '/unchecked/' + rev + '/' + key

    return check

def get_check_rev():
    rev_file = get_tcheck_dir() + '/refs/' + get_branch_shorthand() + '/REV'
    rev = '1'
    
    if os.path.exists(rev_file):
        f = open(rev_file, 'r')
        rev = f.read()
    
    return rev

def get_all_statuses():
    tasks_dir = get_tasks_dir()
    try:
        tasks = os.listdir(tasks_dir)
    except FileNotFoundError:
        raise TcheckException(TcheckErrors.not_init())

    return tasks

def get_status_from_key(key):
    tasks_dir = get_tasks_dir()
    for dir in get_all_statuses():
        task_dir = tasks_dir + '/' + dir + '/' + get_check_rev() + '/' + key
        if os.path.exists(task_dir):
            return dir

def is_tcheck():
    if os.path.exists(get_tcheck_dir()):
        return True

    return False

