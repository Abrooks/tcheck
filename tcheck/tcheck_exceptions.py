import sys

PY2 = sys.version_info[0] == 2

class TcheckException(Exception):
    exit_code = 1

    def __init__(self, message):
        ctor_msg = message
        if PY2:
            if ctor_msg is not None:
                ctor_msg = ctor_msg.encode('utf-8')
        Exception.__init__(self, ctor_msg)
        self.message = message
    
    def format_message(self):
        return self.message

    def __str__(self):
        return self.message

    if PY2: 
        __unicode__ = __str__

        def __str__(self):
            return self.message.encode('utf-8')

