import pygit2
import os
import subprocess
import re

try:
    from . import tcheck_check
except ImportError:
    import tcheck.tcheck_check

from tcheck.tcheck_exceptions import TcheckException
from tcheck.tcheck_errors import TcheckErrors

try:
        FileNotFoundError
except NameError:
        FileNotFoundError = IOError

def get_repo():
    try:
        repo_path = pygit2.discover_repository(os.getcwd())
        repo = pygit2.Repository(repo_path)
        return repo
    except AttributeError as e:
        raise TcheckException(e)

def get_head():
    try:
        repo = get_repo()
        head = repo.lookup_reference('HEAD').resolve()
        head = repo.head
        return head
    except (KeyError, AttributeError, NameError):
        return 'master'

def has_repo():
    head = get_head()
    if head:
        return True
    return False

def get_workdir():
    repo = get_repo()
    try:
        return repo.workdir
    except (AttributeError, NameError):
        return ''

def get_branch_shorthand():
    head = get_head()
    try:
        return head.shorthand
    except (AttributeError, NameError):
        return ''

def write_head_from_git(tcheck_dir):
    branch_name = get_branch_shorthand()
    head = tcheck_dir + '/HEAD'
    with open(head, 'w') as fhead:
        fhead.write(branch_name)

def write_hooks(repo=False):
    repo_path = get_repo()
    repo_path = repo_path.path
    if repo: 
        repo_path = repo
    
    try:
        add_hook = repo_path + 'hooks/pre-commit'
        try:
            with open(add_hook, 'r') as hook:
                old_hook = hook.read()
        except:
            old_hook = ''

        hook_found = old_hook.find('tcheck hooks pre-commit')

        if hook_found > -1:
            already_has_hook = True
        else:
            already_has_hook = False

        if not already_has_hook:
            hook_content = 'tcheck hooks pre-commit' + '\n'
            hook_content += old_hook
            with open(add_hook, 'w') as file: 
                file.write(hook_content)
                os.chmod(add_hook, 0o777)

    except:
        raise TcheckException(TcheckErrors.precommit_write())

def get_modified_files():
    try:
        files = subprocess.run(['git', 'status', '--porcelain', '--untracked-files'], stdout=subprocess.PIPE)
        files = files.stdout.decode('utf-8').split()
    except AttributeError:
        files = []

    return get_every_other(files)

def pre_commit():
    modified_files = get_modified_files()

    for modified_file in modified_files:
        try:
            with open(modified_file, 'r') as file:
                file_content = file.read()
                keys = tcheck_check.do_match(file_content)
        except FileNotFoundError:
            pass

    os.system('tcheck list')

def get_every_other(items):
    return items[1:2]

