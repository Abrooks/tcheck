import yaml
import os
from click import echo

config = os.path.expanduser('~/.config/tcheck/checks.yaml')

try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError
   
def get_config(config=config):
    try:
        with open(config, 'r') as file:
            data = yaml.load(file, Loader=yaml.BaseLoader)
            return data
    except FileNotFoundError:
        return False

