import yaml
import os
import re
import click

from tcheck.tcheck_exceptions import TcheckException
from tcheck.tcheck_util import is_tcheck
from tcheck.tcheck_errors import TcheckErrors

try:
    from . import tcheck_util as util
except ImportError:
    import tcheck.tcheck_util as util

try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError

def add(key, match, run, message):
    file = util.get_check(key)

    if os.path.exists(file):
        raise TcheckException(TcheckErrors.check_exists())

    data = {
        'match'  : match,
        'run'    : run,
        'message': message
    }

    with open(file, 'w') as check:
        yaml.dump(data, check, default_flow_style=False)

def delete(key):
    check = util.get_check(key)

    if os.path.isfile(check):
        os.remove(check)
    else:
        raise TcheckException(TcheckErrors.check_not_exists())

def uncheck(key):
    check = util.get_check(key)
    unchecked_check = util.get_tasks_dir() + '/unchecked/' + util.get_check_rev() + '/' + key
    if os.path.isfile(check):
        os.rename(check, unchecked_check)
    else:
        raise TcheckException(TcheckErrors.check_not_exists())

def fail(key):
    check = util.get_check(key)
    failed_check = util.get_tasks_dir() + '/failed/' + util.get_check_rev() + '/' + key
    if os.path.exists(check):
        os.rename(check, failed_check)
    else:
        raise TcheckException(TcheckErrors.check_not_exists())

def complete(key):
    check = util.get_check(key)
    complete_check = util.get_tasks_dir() + '/complete/' + util.get_check_rev() + '/' + key
    if os.path.exists(check):
        os.rename(check, complete_check)
    else:
        raise TcheckException(TcheckErrors.check_not_exists())

def list():
    if not os.path.exists(util.get_tasks_dir()):
        raise TcheckException(TcheckErrors.not_init())
    check = util.get_tasks_dir() + '/unchecked/' + util.get_check_rev() + '/'
    failed_check = util.get_tasks_dir() + '/failed/' + util.get_check_rev() + '/'
    complete_check = util.get_tasks_dir() + '/complete/' + util.get_check_rev() + '/'

    click.echo('Rev:' + util.get_check_rev() + '\n')

    try:
        if os.listdir(check):
            click.echo('Unchecked:')
            for file in os.listdir(check):
                click.echo(file + ' ' + get_message(file))

        if os.listdir(failed_check):
            click.echo('\n')
            click.echo('Failed:')
            for file in os.listdir(failed_check):
                click.secho(file + ' ' + get_message(file), fg='red')

        if os.listdir(complete_check):
            click.echo('\n')
            click.echo('Complete:')
            for file in os.listdir(complete_check):
                click.secho(file + ' ' + get_message(file), fg='green')
    except FileNotFoundError:
        pass

def run(key):
    file = util.get_check(key)
    if not os.path.isfile(file):
        raise TcheckException(TcheckErrors.check_not_exists())
    with open(file, 'r') as check:
        data = yaml.load(check, Loader=yaml.BaseLoader)
        data.setdefault('run', 'echo "nothing to run"')
        print('Running:', data['run'])
        os.system(data['run'])

def get_message(key):
    file = util.get_check(key)

    with open(file, 'r') as check:
        data = yaml.load(check, Loader=yaml.BaseLoader)
        data.setdefault('message', 'No message set')
        return data['message']

def get_match(key):
    file = util.get_check(key)

    with open(file, 'r') as check:
        data = yaml.load(check, yaml.BaseLoader)
        data.setdefault('match', '')
        return data['match']

def get_run(key):
    file = util.get_check(key)

    with open(file, 'r') as check:
        data = yaml.load(check, Loader=yaml.BaseLoader)
        data.setdefault('run', '')
        return data['run']

def do_match(content):
    matches = []
    for key in get_all_checks():
        has_match = False
        match = get_match(key)

        if match:
            try:
                matches = re.search(match, content)
                if matches:
                    has_match = True
            except TypeError:
                for single_match in match:
                    matches = re.search(single_match, content)
                    if matches:
                        has_match = True

        if has_match:
            uncheck(key)
            try:
                matches.append(key)
            except AttributeError:
                matches = [key]

    return matches

def do_run(key):
    run = get_run(key)
    print('Running:', run)

    os.system(run)
    complete(key)

def do_run_unchecked():
    check = util.get_tasks_dir() + '/unchecked/' + util.get_check_rev() + '/'

    for key in os.listdir(check):
        do_run(key)

def get_all_checks(ctx = False, args = False, incomplete = False):
    tasks_dir = util.get_tasks_dir()
    checks = []
    keys = []
    for status in util.get_all_statuses():
        dir = tasks_dir + '/' + status + '/' + util.get_check_rev()
        for check in os.listdir(dir):
            checks.append(check)

    for key in checks:
        if str(incomplete) in key:
            keys.append(key)
    
    if keys:
        return keys
    else:
        return checks

def get_failed_or_unchecked(ctx='', args='', incomplete=''):
    all_keys = []
    keys = []
    check = util.get_tasks_dir() + '/unchecked/' + util.get_check_rev() + '/'
    failed_check = util.get_tasks_dir() + '/failed/' + util.get_check_rev() + '/'

    for key in os.listdir(check):
        all_keys.append(key)
        if str(incomplete) in key:
            keys.append(key)

    for key in os.listdir(failed_check):
        all_keys.append(key)
        if str(incomplete) in key:
            keys.append(key)

    if keys:
        return keys
    else:
        return all_keys

def get_complete_or_unchecked(ctx='', args='', incomplete=''):
    all_keys = []
    keys = []
    check = util.get_tasks_dir() + '/unchecked/' + util.get_check_rev() + '/'
    complete_check = util.get_tasks_dir() + '/complete/' + util.get_check_rev() + '/'

    for key in os.listdir(check):
        all_keys.append(key)
        if str(incomplete) in key:
            keys.append(key)

    for key in os.listdir(complete_check):
        all_keys.append(key)
        if str(incomplete) in key:
            keys.append(key)
    
    if keys:
        return keys
    else:
        return all_keys

def get_complete_or_failed(ctx='', args='', incomplete=''):
    all_keys = []
    keys = []
    failed_check = util.get_tasks_dir() + '/failed/' + util.get_check_rev() + '/'
    complete_check = util.get_tasks_dir() + '/complete/' + util.get_check_rev() + '/'

    for key in os.listdir(failed_check):
        all_keys.append(key)
        if str(incomplete) in key:
            keys.append(key)

    for key in os.listdir(complete_check):
        all_keys.append(key)
        if str(incomplete) in key:
            keys.append(key)

    if keys:
        return keys
    else:
        return all_keys

