import os
import click
import shutil

from tcheck.tcheck_exceptions import TcheckException
from tcheck.tcheck_errors import TcheckErrors
from tcheck.tcheck_util import get_tcheck_dir, get_tasks_dir, get_check_rev

from . import tcheck_config as config
from . import tcheck_git as git 
from . import tcheck_check as tcheck_check

def do_init(use_config = True, config_file=False):
    if not git.has_repo():
        raise TcheckException(TcheckErrors.not_repo())

    dir = get_tcheck_dir()

    if (os.path.exists(dir)):
        raise TcheckException(TcheckErrors.already_init())
    else:
        os.makedirs(dir)
    
    git.write_head_from_git(get_tcheck_dir());

    rev_file = get_tcheck_dir() + '/refs/' + git.get_branch_shorthand() + '/REV'
    
    if not os.path.exists(rev_file):
        update_check_rev()

    tasks_dir = get_tasks_dir()
    rev = get_check_rev()

    if use_config:
        if config_file:
            task_config = config.get_config(config_file)
        else:
            task_config = config.get_config()

        try:
            checks = task_config['checks']
        
            for key in checks:
                checks[key].setdefault('match', '')
                checks[key].setdefault('run', '')
                checks[key].setdefault('message', '')

                match = checks[key]['match']
                run = checks[key]['run']
                message = checks[key]['message']

                tcheck_check.add(key, match, run, message)
        except TypeError:
            pass
        except Exception as e:
            raise TcheckException(e)

    git.write_hooks()

def do_remove():
    dir = get_tcheck_dir()
    shutil.rmtree(dir)

def update_check_rev(rev=False, uncheck_all=False):
    rev_file = get_tcheck_dir() + '/refs/' + git.get_branch_shorthand() + '/REV'
    old_rev = False
    copy_dirs = True
    if os.path.exists(rev_file):
        with open(rev_file, 'r') as f:
            old_rev = f.read()

        new_rev = int(old_rev) + 1
        if not rev:
            rev = str(new_rev)

    if rev:
        if os.path.exists(rev_file):
            os.remove(rev_file)
    else:
        rev = '1'

    tasks_dir = get_tasks_dir()

    if os.path.exists(tasks_dir):
        with open(rev_file, 'w') as fhead:
            fhead.write(rev)
    else:
        copy_dirs = False
        if (old_rev):
            rev = str(old_rev)
        else:
            rev = '1'

    if old_rev and copy_dirs:
        shutil.copytree(tasks_dir + '/unchecked/' + old_rev, tasks_dir + '/unchecked/' + rev)

        failed_dir = tasks_dir + '/failed/' 
        failed_checks = os.listdir(failed_dir + old_rev)
        if not os.path.exists(failed_dir + rev):
            os.makedirs(failed_dir + rev)

        for f in failed_checks:
            from_file = failed_dir + old_rev + '/' + f
            to_file = tasks_dir + '/unchecked/' + rev + '/' + f
            shutil.copyfile(from_file, to_file)

        complete_dir = tasks_dir + '/complete/' 
        complete_checks = os.listdir(complete_dir + old_rev)
        if not os.path.exists(complete_dir + rev):
            os.makedirs(complete_dir + rev)

        if uncheck_all:
            status = 'unchecked'
        else:
            status = 'complete'

        for f in complete_checks:
            from_file = complete_dir + old_rev + '/' + f
            to_file = tasks_dir + '/' + status + '/' + rev + '/' + f
            shutil.copyfile(from_file, to_file)
    else:
        os.makedirs(tasks_dir + '/failed/' + rev)
        os.makedirs(tasks_dir + '/complete/' + rev)
        os.makedirs(tasks_dir + '/unchecked/' + rev)

        with open(rev_file, 'w') as fhead:
            fhead.write(rev)

