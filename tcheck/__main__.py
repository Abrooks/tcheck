#!/usr/bin/env python
import click
import os
import tcheck.tcheck_config as config
import tcheck.tcheck_check as checks
import tcheck.tcheck_git as git
from tcheck.tcheck_exceptions import TcheckException

from tcheck.tcheck_sys import update_check_rev, do_init
from tcheck.tcheck_util import get_tcheck_dir, get_tasks_dir, get_check_rev

def tcheck_call(callable, *args, **kwargs):
    try:
        return callable(*args, **kwargs)
    except TcheckException as e:
        raise click.ClickException(e)

@click.group()
def main():
    pass

@main.command()
@click.argument('rev')
def set_rev(rev):
    tcheck_call(update_check_rev, rev)
 
@main.command()
@click.option('--uncheck-all', '-u', is_flag=True)
def new_rev(uncheck_all):
   tcheck_call(update_check_rev, uncheck_all=uncheck_all)

@main.command()
def init():
    tcheck_call(do_init)

@main.group()
def run():
    pass

@run.command()
def unchecked():
    tcheck_call(checks.do_run_unchecked)

@main.group()
def hooks():
    pass

@hooks.command()
def pre_commit():
    tcheck_call(git.pre_commit)

@main.command()
def list():
    tcheck_call(checks.list)

@main.group()
def check():
    pass

@check.command()
@click.argument('key', autocompletion=checks.get_all_checks)
def get_message(key):
    click.echo(checks.get_message(key))

@check.command()
@click.argument('key')
@click.argument('message')
@click.option('--match', '-m', default='')
@click.option('--run', '-r', default='')
def add(key, match, run, message):
    tcheck_call(checks.add, key, match, run, message)

@check.command()
@click.argument('keys', autocompletion=checks.get_all_checks, nargs=-1)
def run(keys):
    for key in keys:
        tcheck_call(checks.run, key)

@check.command()
@click.argument('keys', autocompletion=checks.get_complete_or_failed, nargs=-1)
def uncheck(keys):
    for key in keys:
        tcheck_call(checks.uncheck, key)

@check.command()
@click.argument('keys', autocompletion=checks.get_all_checks, nargs=-1)
def delete(keys):
    for key in keys:
        tcheck_call(checks.delete, key)

@check.command()
@click.argument('keys', autocompletion=checks.get_complete_or_unchecked, nargs=-1)
def fail(keys):
    for key in keys:
        tcheck_call(checks.fail, key)

@check.command()
@click.argument('keys', autocompletion=checks.get_failed_or_unchecked, nargs=-1)
def complete(keys):
    for key in keys:
        tcheck_call(checks.complete, key)

if __name__ == '__main__':
    main()

