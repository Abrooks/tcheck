class TcheckErrors():
    message_check_not_exist = 'Check does not exist'
    message_check_exists = 'Check already exists'
    message_not_init = 'Tcheck has not be initialized'
    message_already_init = 'Tcheck has already been initialized'
    message_precommit_write = 'Could not write pre-commit hook'
    message_not_repo = 'Not a git repo'

    @staticmethod
    def check_not_exists():
        return TcheckErrors.message_check_not_exist

    @staticmethod
    def check_exists():
        return TcheckErrors.message_check_exists

    @staticmethod
    def not_init():
        return TcheckErrors.message_not_init
    
    @staticmethod
    def precommit_write():
        return TcheckErrors.message_precommit_write
    
    @staticmethod
    def not_repo():
        return TcheckErrors.message_not_repo

    @staticmethod
    def already_init():
        return TcheckErrors.message_already_init

